#include "break/pnsv/Pensieve.h"
#include "break/pnsv/Tree.h"

#include <mn/Str.h>
#include <mn/File.h>

namespace brk::pnsv
{
	using namespace mn;

	struct IPensieve
	{
		Str filename;
		File file;
		Tree tree;
	};

	inline static void
	file_push8(File file, uint8_t v)
	{
		size_t s = file_write(file, block_from(v));
		assert(s == sizeof(v));
	}

	inline static void
	file_push16(File file, uint16_t v)
	{
		size_t s = file_write(file, block_from(v));
		assert(s == sizeof(v));
	}

	inline static void
	file_push32(File file, uint32_t v)
	{
		size_t s = file_write(file, block_from(v));
		assert(s == sizeof(v));
	}

	inline static void
	file_push64(File file, uint64_t v)
	{
		size_t s = file_write(file, block_from(v));
		assert(s == sizeof(v));
	}

	inline static uint8_t
	file_pop8(File file)
	{
		uint8_t v = 0;
		size_t s = file_read(file, block_from(v));
		assert(s == sizeof(v));
		return v;
	}

	inline static uint16_t
	file_pop16(File file)
	{
		uint16_t v = 0;
		size_t s = file_read(file, block_from(v));
		assert(s == sizeof(v));
		return v;
	}

	inline static uint32_t
	file_pop32(File file)
	{
		uint32_t v = 0;
		size_t s = file_read(file, block_from(v));
		assert(s == sizeof(v));
		return v;
	}

	inline static uint64_t
	file_pop64(File file)
	{
		uint64_t v = 0;
		size_t s = file_read(file, block_from(v));
		assert(s == sizeof(v));
		return v;
	}

	inline static void
	_pensieve_remove(IPensieve* self, Node* node)
	{
		assert(node->kind == Node::KIND_FILE);

		file_cursor_move_to_start(self->file);
		file_cursor_move(self->file, node->file.data_pos);

		uint64_t size = file_pop64(self->file);
		size |= 0x8000000000000000; //set the sign bit

		file_cursor_move(self->file, -8);
		file_push64(self->file, size);
	}

	inline static void
	_pensieve_parse(IPensieve* self)
	{
		file_cursor_move_to_start(self->file);
		int64_t size = file_size(self->file);
		Str name = str_new();
		while(true)
		{
			//get the current position in the file
			int64_t pos = file_cursor_pos(self->file);
			if(pos >= size) break;

			//get the section name
			uint16_t name_len = file_pop16(self->file);
			str_resize(name, name_len);
			size_t s = file_read(self->file, block_from(name));
			assert(s == size_t(name_len));

			//get the section data info
			uint64_t data_info = file_pop64(self->file);
			bool removed = data_info & 0x8000000000000000;
			uint64_t data_size = data_info & (~0x8000000000000000);

			//rebuild the tree
			if(removed == false)
				tree_file_add(self->tree, name, pos, file_cursor_pos(self->file), data_size);

			//skip the data
			file_cursor_move(self->file, data_size);
		}
		str_free(name);
	}


	//API
	Pensieve
	pensieve_file_new(const char* filename)
	{
		IPensieve* self = alloc<IPensieve>();
		self->filename = str_from_c(filename);
		bool file_exists = path_exists(self->filename);
		if(file_exists)
			self->file = file_open(filename, IO_MODE::READ_WRITE, OPEN_MODE::OPEN_ONLY);
		else
			self->file = file_open(filename, IO_MODE::READ_WRITE, OPEN_MODE::CREATE_ONLY);
		self->tree = tree_new();
		if(file_exists) _pensieve_parse(self);
		return (Pensieve)self;
	}

	void
	pensieve_free(Pensieve pensieve)
	{
		IPensieve* self = (IPensieve*)pensieve;
		str_free(self->filename);
		file_close(self->file);
		tree_free(self->tree);
		free(self);
	}

	void
	pensieve_write(Pensieve pensieve, const Str& name, const Block& data)
	{
		IPensieve* self = (IPensieve*)pensieve;

		if(Node* node = tree_file_find(self->tree, name); node)
		{
			_pensieve_remove(self, node);
			tree_file_remove(self->tree, name);
		}

		file_cursor_move_to_end(self->file);
		int64_t pos = file_cursor_pos(self->file);

		assert(name.count < UINT16_MAX);
		file_push16(self->file, (uint16_t)name.count);
		size_t s = file_write(self->file, block_from(name));
		assert(s == name.count);
		file_push64(self->file, (uint64_t)data.size);

		int64_t data_pos = file_cursor_pos(self->file);
		s = file_write(self->file, data);
		assert(s == data.size);

		tree_file_add(self->tree, name, pos, data_pos, data.size);
	}

	bool
	pensieve_exists(Pensieve pensieve, const Str& name)
	{
		IPensieve* self = (IPensieve*)pensieve;
		return tree_file_exists(self->tree, name);
	}

	void
	pensieve_remove(Pensieve pensieve, const Str& name)
	{
		IPensieve* self = (IPensieve*)pensieve;
		if(Node* node = tree_file_find(self->tree, name))
		{
			_pensieve_remove(self, node);
			tree_file_remove(self->tree, name);
		}
	}

	Str
	pensieve_read(Pensieve pensieve, const Str& name, Allocator allocator)
	{
		IPensieve* self = (IPensieve*)pensieve;
		Str res = str_with_allocator(allocator);
		if(Node* node = tree_file_find(self->tree, name))
		{
			file_cursor_move_to_start(self->file);
			file_cursor_move(self->file, node->file.data_pos);
			str_resize(res, node->file.size);
			size_t s = file_read(self->file, block_from(res));
			assert(s == node->file.size);
		}
		return res;
	}
}
