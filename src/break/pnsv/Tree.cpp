#include "break/pnsv/Tree.h"
#include "break/pnsv/Node.h"
#include "break/pnsv/Path_Lexer.h"

#include <mn/Str_Intern.h>
#include <mn/Pool.h>
#include <mn/Buf.h>
#include <mn/Memory.h>

#include <assert.h>

namespace brk::pnsv
{
	using namespace mn;

	struct ITree
	{
		Str_Intern str_table;
		Pool nodes_pool;
		Node* root;
		Buf<const char*> path;
	};

	inline static Node*
	_tree_folder_new(ITree* self, const char* name)
	{
		Node* node = (Node*)pool_get(self->nodes_pool);
		node->kind = Node::KIND_FOLDER;
		node->name = name;
		node->folder.children = buf_new<Node*>();
		return node;
	}

	inline static Node*
	_tree_folder_search(Node* node, const char* name)
	{
		assert(node->kind == Node::KIND_FOLDER);
		for(Node* child: node->folder.children)
			if(child->name == name)
				return child;
		return nullptr;
	}

	inline static void
	_tree_folder_remove(ITree* self, Node* node, const char* name)
	{
		assert(node->kind == Node::KIND_FOLDER);
		for(size_t i = 0; i < node->folder.children.count; ++i)
		{
			if(Node* file = node->folder.children[i]; file->name == name)
			{
				buf_remove(node->folder.children, i);
				pool_put(self->nodes_pool, file);
				return;
			}
		}
	}

	inline static Node*
	_tree_file_new(ITree* self, const char* name, int64_t section_pos, int64_t data_pos, size_t size)
	{
		Node* node = (Node*)pool_get(self->nodes_pool);
		node->kind = Node::KIND_FILE;
		node->name = name;
		node->file.section_pos = section_pos;
		node->file.data_pos = data_pos;
		node->file.size = size;
		return node;
	}

	inline static void
	_tree_node_free(ITree* self, Node* node)
	{
		switch(node->kind)
		{
			case Node::KIND_FILE: break;
			case Node::KIND_FOLDER:
				for(Node* n: node->folder.children)
					_tree_node_free(self, n);
				buf_free(node->folder.children);
				break;
			default: assert(false && "unreachable"); break;
		}
		pool_put(self->nodes_pool, node);
	}

	inline static void
	_tree_parse_path(ITree* self, const mn::Str& path)
	{
		buf_clear(self->path);

		Path_Lexer lexer = path_lexer_new(path);
		while(true)
		{
			if(Path_Token tkn = path_lexer_next(lexer); tkn)
				buf_push(self->path, str_intern(self->str_table, tkn.begin, tkn.end));
			else
				break;
		}
	}


	//API
	Tree
	tree_new()
	{
		ITree* self = alloc<ITree>();
		self->str_table = str_intern_new();
		self->nodes_pool = pool_new(sizeof(Node), 64);
		self->root = _tree_folder_new(self, str_intern(self->str_table, "/"));
		self->path = buf_new<const char*>();
		return (Tree)self;
	}

	void
	tree_free(Tree tree)
	{
		ITree* self = (ITree*)tree;
		_tree_node_free(self, self->root);
		str_intern_free(self->str_table);
		pool_free(self->nodes_pool);
		buf_free(self->path);
		free(self);
	}

	Node*
	tree_root(Tree tree)
	{
		ITree* self = (ITree*)tree;
		return self->root;
	}

	void
	tree_file_add(Tree tree, const mn::Str& path, int64_t section_pos, int64_t data_pos, size_t size)
	{
		ITree* self = (ITree*)tree;
		
		_tree_parse_path(self, path);
		assert(self->path.count > 0);

		Node* it = self->root;

		size_t i = 0;
		if(self->path[0] == it->name)
			i == 1;
		for(; i < self->path.count - 1; ++i)
		{
			const char* name = self->path[i];
			
			assert(it->kind == Node::KIND_FOLDER);
			if(Node* next_it = _tree_folder_search(it, name); next_it == nullptr)
			{
				next_it = _tree_folder_new(self, name);
				buf_push(it->folder.children, next_it);
				it = next_it;
			}
			else
			{
				it = next_it;
			}
		}

		//this should be folder remember that we are adding a file and it's only legal to add file to a folder
		assert(it->kind == Node::KIND_FOLDER);
		assert(i == self->path.count - 1 && "You can't create a file name '/'");
		if(i == self->path.count - 1)
		{
			const char* name = buf_top(self->path);
			buf_push(it->folder.children, _tree_file_new(self, name, section_pos, data_pos, size));
		}
	}

	Node*
	tree_node_find(Tree tree, const mn::Str& path, Node::KIND kind)
	{
		ITree* self = (ITree*)tree;
		if(self->path.count == 0)
			return nullptr;

		Node* it = self->root;

		size_t i = 0;
		if(self->path[0] == it->name)
			i = 1;

		for(;i < self->path.count; ++i)
			if(it = _tree_folder_search(it, self->path[i]); it == nullptr)
				return nullptr;

		if(it->kind != kind)
			return nullptr;
		return it;
	}

	void
	tree_file_remove(Tree tree, const mn::Str& path)
	{
		ITree* self = (ITree*)tree;

		_tree_parse_path(self, path);
		if(self->path.count == 0)
			return;

		Node* it = self->root;
		for(size_t i = 0; i < self->path.count - 1; ++i)
			if(it = _tree_folder_search(it, self->path[i]); it == nullptr)
				return;

		if(it->kind != Node::KIND_FOLDER)
			return;

		_tree_folder_remove(self, it, buf_top(self->path));
	}
}
