mn = path.getabsolute("external/mn")

workspace "pensieve"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	startproject "unittest"
	defaultplatform "x64"

	group "External"
		include "external/mn/mn"

	group ""
		include "pnsv"
		include "unittest"
