#pragma once

#include <mn/Str.h>

namespace brk::pnsv
{
	struct Path_Token
	{
		const char* begin;
		const char* end;

		inline operator bool() const { return begin != nullptr && end != nullptr; }
	};

	struct Path_Lexer
	{
		const char* it;
		int32_t c;
	};

	inline static Path_Lexer
	path_lexer_new(const char* path)
	{
		Path_Lexer self{};
		self.it = path;
		self.c = mn::rune_read(self.it);
		return self;
	}

	inline static Path_Lexer
	path_lexer_new(const mn::Str& str)
	{
		return path_lexer_new(str.ptr);
	}

	inline static bool
	path_lexer_eof(Path_Lexer& self)
	{
		return self.c == 0;
	}

	inline static bool
	path_lexer_read(Path_Lexer& self)
	{
		if(path_lexer_eof(self))
			return false;
		self.it = mn::rune_next(self.it);
		self.c = mn::rune_read(self.it);
		return true;
	}

	inline static void
	path_lexer_skip(Path_Lexer& self)
	{
		while(self.c == '/')
			if(path_lexer_read(self) == false)
				break;
	}

	inline static Path_Token
	path_lexer_next(Path_Lexer& self)
	{
		if(path_lexer_eof(self))
			return Path_Token{};

		Path_Token tkn{};

		//first / should be a separate token
		if(self.c == '/')
		{
			//set the begining 
			tkn.begin = self.it;
			//read the /
			path_lexer_read(self);
			//set the end
			tkn.end = self.it;

			//always skip the separator at the end of the lexer_next so that
			//next call to the same function will have self.c != '/'
			path_lexer_skip(self);
			return tkn;
		}

		tkn.begin = self.it;
		while(self.c != '/')
			if(path_lexer_read(self) == false)
				break;
		tkn.end = self.it;

		path_lexer_skip(self);
		return tkn;
	}
}
