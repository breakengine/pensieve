#pragma once

#if defined(OS_WINDOWS)
	#if defined(PNSV_DLL)
		#define API_PNSV __declspec(dllexport)
	#else
		#define API_PNSV __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_PNSV 
#endif