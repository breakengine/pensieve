#pragma once

#include "break/pnsv/Exports.h"

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::pnsv
{
	MS_HANDLE(Pensieve);

	API_PNSV Pensieve
	pensieve_file_new(const char* filename);

	inline static Pensieve
	pensieve_file_new(const mn::Str& filename)
	{
		return pensieve_file_new(filename.ptr);
	}

	API_PNSV void
	pensieve_free(Pensieve pensieve);

	inline static void
	destruct(Pensieve pensieve)
	{
		pensieve_free(pensieve);
	}

	API_PNSV void
	pensieve_write(Pensieve pensieve, const mn::Str& name, const mn::Block& data);

	inline static void
	pensieve_write(Pensieve pensieve, const char* name, const mn::Block& data)
	{
		pensieve_write(pensieve, mn::str_lit(name), data);
	}

	API_PNSV bool
	pensieve_exists(Pensieve pensieve, const mn::Str& name);

	inline static bool
	pensieve_exists(Pensieve pensieve, const char* name)
	{
		return pensieve_exists(pensieve, mn::str_lit(name));
	}

	API_PNSV void
	pensieve_remove(Pensieve pensieve, const mn::Str& name);

	inline static void
	pensieve_remove(Pensieve pensieve, const char* name)
	{
		return pensieve_remove(pensieve, mn::str_lit(name));
	}

	API_PNSV mn::Str
	pensieve_read(Pensieve pensieve, const mn::Str& name, mn::Allocator allocator = mn::allocator_top());

	inline static mn::Str
	pensieve_read(Pensieve pensieve, const char* name, mn::Allocator allocator = mn::allocator_top())
	{
		return pensieve_read(pensieve, mn::str_lit(name), allocator);
	}
}