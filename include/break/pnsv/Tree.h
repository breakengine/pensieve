#pragma once

#include "break/pnsv/Exports.h"
#include "break/pnsv/Node.h"

#include <mn/Base.h>
#include <mn/Str.h>

namespace brk::pnsv
{
	MS_HANDLE(Tree);

	API_PNSV Tree
	tree_new();

	API_PNSV void
	tree_free(Tree tree);

	inline static void
	destruct(Tree tree)
	{
		tree_free(tree);
	}

	API_PNSV Node*
	tree_root(Tree tree);

	API_PNSV void
	tree_file_add(Tree tree, const mn::Str& path, int64_t section_pos, int64_t data_pos, size_t size);

	API_PNSV Node*
	tree_node_find(Tree tree, const mn::Str& path, Node::KIND kind);

	inline static Node*
	tree_file_find(Tree tree, const mn::Str& path)
	{
		return tree_node_find(tree, path, Node::KIND_FILE);
	}

	inline static bool
	tree_file_exists(Tree tree, const mn::Str& path)
	{
		return tree_file_find(tree, path) != nullptr;
	}

	API_PNSV void
	tree_file_remove(Tree tree, const mn::Str& path);

	inline static Node*
	tree_folder_find(Tree tree, const mn::Str& path)
	{
		return tree_node_find(tree, path, Node::KIND_FOLDER);
	}

	inline static bool
	tree_folder_exists(Tree tree, const mn::Str& path)
	{
		return tree_folder_find(tree, path) != nullptr;
	}
}