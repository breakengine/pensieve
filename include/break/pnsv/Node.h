#pragma once

#include <mn/Buf.h>

namespace brk::pnsv
{
	struct Node
	{
		enum KIND
		{
			KIND_NONE,
			KIND_FILE,
			KIND_FOLDER
		};

		KIND kind;
		const char* name;
		union
		{
			struct
			{
				int64_t section_pos;
				int64_t data_pos;
				size_t size;
			} file;

			struct
			{
				mn::Buf<Node*> children;
			} folder;
		};
	};
}